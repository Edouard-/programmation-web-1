Exercice - CSS3 avatar 2

Objectif:
    Utilisation des fonctions CSS3 pour l'intégration des images fluides.

Consignes:
    - Crée le fichier css/css-avatar-2.css pour obtenir le résultat des maquettes
    - Voir la présentation du professeur.
